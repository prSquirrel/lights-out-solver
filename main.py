import LightsOut


def main():
    lamps = [[1, 0, 0, 0, 0],
             [0, 0, 1, 0, 0],
             [0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0]]
    solver = LightsOut.Solver(lamps)
    print(solver.solve())

if __name__ == '__main__':
    main()
