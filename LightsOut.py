import numpy as np


def swap_rows(arr, frm, to):
    arr[[frm, to], :] = arr[[to, frm], :]


class Solver(object):
    def __init__(self, state):
        self.state_matrix = np.matrix(state)
        self.rows = np.size(self.state_matrix, axis=0)
        self.columns = np.size(self.state_matrix, axis=1)

        self._augmented_matrix = None

    def _get_adjacent_indices(self, position):
        results = []
        # generate list of offsets
        # for adjacency in such form(skip corners):
        #               X
        #              XXX
        #               X
        for i, j in [(i, j) for i in (-1, 0, 1) for j in (-1, 0, 1) if abs(i) + abs(j) < 2]:
            x, y = position[0] + i, position[1] + j
            if 0 <= x < self.rows and 0 <= y < self.columns:
                results.append((x, y))
        return results

    def _generate_equations_matrix(self):
        equation_matrix = np.zeros((self.rows ** 2, self.columns ** 2), dtype=np.int)
        for (i, j), elem in np.ndenumerate(self.state_matrix):
            # treat current index in such form as if it were a column vector
            a_ij = i * self.columns + j
            # loop over generated list of adjacent cells
            for u, v in self._get_adjacent_indices((i, j)):
                b_uv = u * self.columns + v
                # mark corresponding cells
                equation_matrix[a_ij, b_uv] = 1
        return equation_matrix

    def _augment_matrix(self):
        state_vector = np.reshape(self.state_matrix, (-1, 1))
        equations = self._generate_equations_matrix()
        self._augmented_matrix = np.concatenate((equations, state_vector), axis=1)

    def _apply_gaussian_elimination(self):
        n = np.size(self._augmented_matrix, axis=0)
        m = np.size(self._augmented_matrix, axis=1)
        for row in range(0, n, 1):
            shift = 1
            while self._augmented_matrix[row, row] == 0 and row + shift < n:
                if self._augmented_matrix[row + shift, row] == 1:
                    swap_rows(self._augmented_matrix, row + shift, row)
                shift += 1
            for elim_row in range(0, n, 1):
                if elim_row != row:
                    pivot = self._augmented_matrix[elim_row, row]
                    for column in range(0, m, 1):
                        # in mod 2 arithmetic 1 - 1 = 0 which is the same as 1 + 1 = 0
                        # so only a xor b is needed
                        self._augmented_matrix[elim_row, column] = self._augmented_matrix[elim_row, column] ^ (
                            self._augmented_matrix[row, column] * pivot)

    def solve(self):
        self._generate_equations_matrix()
        self._augment_matrix()
        self._apply_gaussian_elimination()
        return np.reshape(self._augmented_matrix[:, -1], (self.rows, self.columns))
